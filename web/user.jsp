<%@ page import="layer.pl.UserDataBean" %>
<%@ page import="layer.pl.GameDataBean" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!
    String colorPassed = "#23d160", colorLocked = "#ffdd57";
    String displayLevel(int level, GameDataBean levelData, boolean playable) {
        String result = "<div class=\"card\">" +
                            "<div class=\"card-header\">" +
                                "<div class=\"card-header-title\">" + level + "</div>" +
                            "</div>" +
                            "<div class=\"card-content\" style=\"background:" +  (playable?colorPassed:colorLocked) + "\">";
        result +=               (levelData!=null?"Time Record:<br>"+levelData.getTimeRecord()+"ms<br>MovesRecord:<br>"+ levelData.getMovesRecord():"No Record<br><br><br><br>");
        result +=           "</div>" +
                "            <div class=\"card-footer\">" +
                "               <a href=\"" + (playable?"level.play?level=" + level:"#") + "\" class=\"card-footer-item\">"+(playable?"Play":"Locked")+"</a>" +
                "            </div></div>";
        return result;
    }
%>
<%
    if(request.getSession().getAttribute("username")==null)
        request.getRequestDispatcher("/index.jsp").forward(request,response);
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title><%= request.getSession().getAttribute("username") %></title>
    <link rel="stylesheet" type="text/css" href="styles/bulma.css">
</head>
<script>
    document.addEventListener('DOMContentLoaded', function () {

        // Get all "navbar-burger" elements
        var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

        // Check if there are any nav burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach(function ($el) {
                $el.addEventListener('click', () => {

                    // Get the target from the "data-target" attribute
                    var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
            });
        }

    });

</script>
<style>
    .card{
        display: inline-block;
        width: 12em;
        margin: 1em;
    }
</style>
<body>
    <div class="container">
        <nav class="navbar">
            <div class="navbar-brand">
                <div class="navbar-item">
                    <strong>Blocky!</strong>
                </div>
                <div class="navbar-burger" data-target="menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="navbar-menu" id="menu">
                <div class="navbar-end">
                    <div class="navbar-item">
                        <a href="/deleteAccount.jsp" class="button is-danger">
                            Delete Account
                        </a>
                    </div>
                    <div class="navbar-item">
                        <a href="user.signout">Sign Out</a>
                    </div>
                    <div class="navbar-item">
                        <a href="/user.scoreboard?until=<%= (int)request.getSession().getAttribute("levels") %>">ScoreBoard</a>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="hero is-primary is-fullheight">
        <div class="hero-body">
            <div class="container">
                <%
                    int levels = (int)request.getSession().getAttribute("levels");
                    UserDataBean userData = (UserDataBean) request.getSession().getAttribute("userData");
                    for (int i = 1; i <= levels; i++) {
                        out.print(displayLevel(i, userData.getGameData(i),(i==1 || userData.getGameData(i)!=null || userData.getGameData(i-1)!=null)));
                    }
                %>
            </div>
        </div>
    </div>
</body>
</html>
