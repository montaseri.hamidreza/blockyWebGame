<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="styles/bulma.css">
    <title>Delete Account</title>
</head>
<body>
    <div class="container" style="margin-top: 2em;">
        <div class="box">
            <div class="message is-danger">
                <div class="message-header">Be careful!</div>
                <p class="message-body">
                    By this action all your data will be lost forever!<br>
                    Enter your username and password for deleting you account.
                </p>
            </div>
            <div class="field">
                <form action="user.delete" method="post">
                    <div class="control">
                        <label class="label">Username</label>
                        <input type="text" name="username">
                    </div>
                    <div class="control">
                        <label class="label">Password</label>
                        <input type="password" name="password">
                    </div>
                    <br>
                    <div class="control"><input type="submit" value="Delete Account" class="button is-danger"></div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>
