<%--
  Created by IntelliJ IDEA.
  User: hamon
  Date: 7/15/17
  Time: 2:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="styles/bulma.css">
      <meta charset="utf-8">
      <title>Welcome!</title>
      <style>
          .box {
              margin: 1em;
          }
      </style>
  </head>
  <body>
    <div class="hero is-large is-primary">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="title is-large is-bold">
                    Blocky!
                </div>
            </div>
        </div>
    </div>

    <div class="hero is-warning">
        <div class="hero-body">
            <%
                if(request.getAttribute("error")!=null) {
                    out.write("<div class='container'><div class='message is-info'>"
                                    + "<div class='message-header'>Alert</div>"
                                    + "<div class='message-body'>" + request.getAttribute("error") + "</div>" +
                                "</div></div>");
                }
            %>
            <div class="columns" style="padding-top: 10px">
                <div class="column"></div>
                <div class="column"></div>
                <div class="column">
                    <div class="box">
                        <h2 class="title">Sign Up</h2>
                        <div class="field">
                            <form action="user.signup" method="post">
                              <label class="label">Username</label>
                                <div class="control"><input type="text" name="username"></div>
                              <label class="label">Password</label>
                                <div class="control"><input type="password" name="password"></div>
                               <label class="label">Confirm Password</label>
                                <div class="control"><input type="password"></div>
                                <div class="control"><input type="submit" value="Sign Up" class="button is-info"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="box">
                        <h2 class="title">Sign In</h2>
                        <div class="field">
                            <form action="user.signin" method="post">
                                <label class="label">Username</label>
                                <div class="control"><input type="text" name="username"></div>
                                <label class="label">Password</label>
                                <div class="control"><input type="password" name="password"></div>
                                <div class="control"><input type="submit" value="Sign In" class="button is-success"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="column"></div>
                <div class="column"></div>
            </div>
            <br>
        </div>
    </div>
  </body>
</html>
