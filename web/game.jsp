<%@ page import="layer.pl.UserDataBean" %>
<%@ page import="layer.pl.GameDataBean" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(request.getSession().getAttribute("username")==null)
        request.getRequestDispatcher("/index.jsp").forward(request,response);
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Level <%= request.getAttribute("level") %></title>
    <link rel="stylesheet" type="text/css" href="styles/bulma.css">
    <link rel="stylesheet" type="text/css" href="styles/customStyle.css">

    <style>
        .tables{margin:auto;}
        tr{height: 3em;}
        .card{width:27em; display:inline-block; margin:1em;}
        .arrow-button{display: table-cell;}
        .game-button{margin:auto;}
        .info{min-width: 5em;}
    </style>
    <script src="scripts/jquery-3.2.1.min.js"></script>
    <script>
        var col = ['#111111','#0074D9','#FF4136','#01FF70','#85144b','#3D9970',' #DDDDDD','#001f3f'];
        var start = new Array(), end = new Array(), cur = new Array();
        var startTime = 0 , moves = 0, level, timeRecord, movesRecord;

        <%
            //Set level
            out.write("level = " + request.getAttribute("level") + ";");
            //Set start and finish maps
            Integer[][] start = ((HashMap<String,Integer[][]>)request.getAttribute("levelData")).get("start");
            Integer[][] end = ((HashMap<String,Integer[][]>)request.getAttribute("levelData")).get("end");
            for(int i = 0; i < 5; i++) {
              out.write("start.push(" + Arrays.toString(Arrays.stream(start[i]).mapToInt(v -> v).toArray()) +");");
              out.write("cur.push(" + Arrays.toString(Arrays.stream(start[i]).mapToInt(v -> v).toArray()) +");");
              out.write("end.push(" + Arrays.toString(Arrays.stream(end[i]).mapToInt(v -> v).toArray()) +");");
            }
            //Set records
            UserDataBean userdata = (UserDataBean)request.getSession().getAttribute("userData");
            GameDataBean gameData = userdata.getGameData((int)request.getAttribute("level"));
            if(gameData==null) {
                out.write("timeRecord = -1, movesRecord = -1;");
            } else {
                out.write("timeRecord = " + gameData.getTimeRecord() + ", movesRecord = " + gameData.getMovesRecord() + ";");
            }
        %>

        function paint(color,table,name) {
            for(i=0; i<5; i++) {
                for(j=0; j<5; j++){
                    var elm = document.getElementById(name+i+j);
                    elm.style.background = color[table[i][j]];
                }
            }
        }

        function left(row) {
            var item = cur[row].shift();
            cur[row].push(item);
            updateMoves();
            paint(col,cur,"cell");
            check();
        }

        function right(row) {
            var item = cur[row].pop();
            cur[row].unshift(item);
            updateMoves();
            paint(col,cur,"cell");
            check();
        }

        function down(column) {
            var item = cur[0][column];
            for(var i=1; i<5; i++) {
                var tmp = cur[i][column];
                cur[i][column] = item;
                item = tmp;
            }
            cur[0][column] = item;
            updateMoves();
            paint(col,cur,"cell");
            check();
        }

        function up(column) {
            var item = cur[4][column];
            for(var i=3; i>=0; i--) {
                var tmp = cur[i][column];
                cur[i][column] = item;
                item = tmp;
            }
            cur[4][column] = item;
            updateMoves();
            paint(col,cur,"cell");
            check();
        }

        function updateMoves() {
            moves++;
            document.getElementById("moves").innerHTML = moves;
        }

        function sendServer(moves, time) {
            var params = {level: level, moves: moves, time: time};
            var res;
            $.post("level.finished", $.param(params), function(response) {
                res = response;
            });
            return res;
        }

        function finish() {
            var time = (new Date() - startTime);
            sendServer(moves, time);
            clearInterval(timeViewIntervalId);

            var tables = document.getElementsByClassName("card");
            for(var i=0; i<tables.length; i++){
                tables[i].classList.add("pulse");
            }

            if(time < timeRecord || timeRecord<0) timeRecord = time;
            if(moves < movesRecord || movesRecord<0) movesRecord = moves;
            document.getElementById("bests").innerHTML = "Time Record: " + timeRecord + "<br>Moves Record: " + movesRecord;
            document.getElementById("now").innerHTML = "Time: " + time + "<br>Moves: " + moves;
            setTimeout(function () {
                document.getElementById("control").classList.toggle("is-active");
            },1000);

        }

        function check() {
            var equ = true;
            for(var i=0; i<5; i++){
                for(var j=0; j<5; j++){
                    if(cur[i][j]!=end[i][j]){
                        equ = false;
                    }
                }
            }
            if(equ == true){
                finish();
            }
        }

        function resetCurrent() {
            for(var i=0; i<5; i++) {
                for(var j=0; j<5; j++) {
                    cur[i][j] = start[i][j];
                }
            }
        }

        function stopGame() {
            clearInterval(timeViewIntervalId);
            document.getElementById("control").classList.toggle("is-active");
        }

        function startGame() {
            resetCurrent();
            paint(col,cur,"cell");
            //reset moves counter
            moves = -1;
            updateMoves();
            //reset timer
            startTime = new Date();
            timeViewIntervalId = setInterval(function () {
                document.getElementById("time").innerHTML = ((new Date() - startTime)/1000).toFixed(2);
            },10);
            //reset view
            var tables = document.getElementsByClassName("card");
            for(var i=0; i<tables.length; i++){
                tables[i].classList.remove("pulse");
            }
            document.getElementById("control").classList.toggle("is-active")
        }

        window.onload = function(){
            if(timeRecord>0 || movesRecord>0)
                document.getElementById("bests").innerHTML = "Time Record: " + timeRecord + "<br>Moves Record: " + movesRecord;
            else
                document.getElementById("bests").innerHTML = "No Records For This Level!";
            paint(col,cur,"cell");
            paint(col,end,"master");
        }
    </script>
</head>
<body>
    <div class="modal is-active" id="control">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title">Level <%= request.getAttribute("level") %></p>
            </header>
            <section class="modal-card-body">
                <div class="level">
                    <div class="level-right has-text-left">
                        <p class="level-item" id="bests"></p>
                    </div>
                    <div class="level-left">
                        <p class="level-item" id="now"></p>
                    </div>
                </div>
            </section>
            <div class="modal-card-foot">
                <a href="user.jsp" class="button game-button is-outlined is-danger">Back To Levels</a>
                <a class="button game-button is-outlined is-success" onclick="startGame()">Start</a>
                <a class="button game-button is-outlined is-info">Next Level</a>
            </div>
        </div>
    </div>
    <div class="container has-text-centered">
        <div class="level" style="margin-top: 3em;">
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Time</p>
                    <p class="title info" id="time">0</p>
                </div>
            </div>
            <p class="level-item has-text-centered">
                <button class="button is-danger is-medium" onclick="stopGame()">Stop</button>
            </p>
            <div class="level-item has-text-centered">
                <div>
                    <p class="heading">Moves</p>
                    <p class="title info" id="moves">0</p>
                </div>
            </div>
        </div>
        <div class="tables">
            <div class="card">
                <div class="card-header">
                    <div class="card-header-title">End</div>
                </div>
                <div class="card-content">
                    <table class="table is-bordered" id="masterTable">
                        <tbody>
                            <%
                                for (int i = 0; i < 7; i++) {
                                    out.write("<tr>");
                                    for (int j = 0; j < 7; j++) {
                                        out.write("<td" + (i>0 && i<6 && j>0 && j<6?" id='master" + (i-1) + (j-1) + "'":"") + "></td>");
                                    }
                                    out.write("</tr>");
                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-header-title">Start</div>
                </div>
                <div class="card-content">
                    <table class="table is-bordered" style="width: 23em;">
                        <tbody>
                        <%
                            out.write("<tr><td></td>");
                            for (int i = 0; i < 5; i++)
                                out.write("<td class='button arrow-button' onclick='up("+i+")'>&#11014;</td>");//out.write("<td><button onclick='up(" + i + ")'></button></td>");
                            out.write("<td></td></tr>");
                            for (int i = 0; i < 5; i++) {
                                out.write("<tr><td class='button arrow-button' onclick='left("+i+")'>&#11013</td>");//out.write("<tr>" + "<td><button onclick='left(" + i + ")'></button></td>");
                                for (int j = 0; j < 5; j++) {
                                    out.write("<td id='cell"+ i + j + "'></td>");
                                }
                                out.write("<td class='button arrow-button' onclick='right("+i+")'>&#10145;</td></tr>");//out.write("<td><button onclick='right(" + i + ")'></button></td>" + "</tr>");
                            }
                            out.write("<tr><td></td>");
                            for (int i = 0; i < 5; i++)
                                out.write("<td class='button arrow-button' onclick='down("+i+")'>&#11015;</td>");//out.write("<td><button onclick='down(" + i + ")'></button></td>");
                            out.write("<td></td></tr>");
                        %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </br>
</body>
</html>