<%@ page import="layer.bl.RecordBean" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Iterator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if(request.getSession().getAttribute("username")==null)
        request.getRequestDispatcher("/index.jsp").forward(request,response);
%>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Scoreboard</title>
    <link rel="stylesheet" type="text/css" href="styles/bulma.css">
</head>
<body>
    <div class="container" style="margin-top: 2em;">
        <form action="user.scoreboard">
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Get top records until:</label>
                </div>
                <div class="field-body">
                    <div class="field has-addons">
                        <div class="control">
                            <div class="select">
                                <select name="until">
                                    <%
                                        for (int i = 1; i <= (int) request.getSession().getAttribute("levels"); i++) {
                                            out.write("<option value='" + i +"'");
                                            if(i==(int)request.getAttribute("untilLevel"))
                                                out.write("selected");
                                            out.write(">level "+ i + "</option>" );
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <div class="control">
                            <input class="button is-info" type="submit" value="Update">
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <br>
        <div>
            <div class="title">Top Records Until Level <%= request.getAttribute("untilLevel") %></div>
            <hr>
            <div class="subtitle">Top Time Records:</div>
            <table class="table is-striped">
                <thead>
                    <th>No</th>
                    <th>Username</th>
                    <th>Time</th>
                </thead>
                <tbody>
                    <%
                        Set<RecordBean> timeRecords = (Set<RecordBean>) request.getAttribute("topTimeRecords");
                        Iterator iterator = timeRecords.iterator();
                        for (int i = 0; i < 3 && iterator.hasNext(); i++) {
                            RecordBean user = (RecordBean) iterator.next();
                            out.write("<tr>" +
                                            "<td>" + (i+1) + "</td>" +
                                            "<td>" + user.getUsername() + "</td>" +
                                            "<td>" + user.getRecord() + "</td>" +
                                    "</tr>");
                        }
                    %>
                </tbody>
            </table>
            <br>
            <hr>
            <div class="subtitle">Top Moves Records:</div>
            <table class="table is-striped">
                <thead>
                <th>No</th>
                <th>Username</th>
                <th>Moves</th>
                </thead>
                <tbody>
                <%
                    timeRecords = (Set<RecordBean>) request.getAttribute("topMovesRecords");
                    iterator = timeRecords.iterator();
                    for (int i = 0; i < 3 && iterator.hasNext(); i++) {
                        RecordBean user = (RecordBean) iterator.next();
                        out.write("<tr>" +
                                "<td>" + (i+1) + "</td>" +
                                "<td>" + user.getUsername() + "</td>" +
                                "<td>" + user.getRecord() + "</td>" +
                                "</tr>");
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>
