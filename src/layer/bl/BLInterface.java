package layer.bl;

import layer.pl.PLException;
import layer.pl.UserDataBean;

import java.util.Map;
import java.util.Set;

public interface BLInterface {
	void register(String username, String password) throws DuplicateUserNameException, SimplePasswordException, PLException;
	UserDataBean login(String username, String password)throws PLException;
	UserDataBean edit(long loginId, String username, String newName, String newFamily)throws PLException;
	void logout(long loginId)throws PLException;
	void unregister(long loginId, String username)throws PLException;
	Map<String,Integer[][]> getLevelData(int level) throws PLException;
    UserDataBean updateLevel(String username, int level, int moves, int time) throws PLException;
	Set<RecordBean> topTimeRecords(int untilLevel) throws PLException;
	Set<RecordBean> topMovesRecord(int untilLevel) throws PLException;
}
