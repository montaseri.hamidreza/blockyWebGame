package layer.bl;

public class RecordBean implements Comparable{
    String username;
    int record;

    public RecordBean(String username, int record) {
        this.username = username;
        this.record = record;
    }

    public String getUsername() {
        return username;
    }

    public int getRecord() {
        return record;
    }

    @Override
    public int compareTo(Object o) {
        return Integer.compare(((RecordBean)o).record,record);
    }
}
