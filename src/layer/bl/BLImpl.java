package layer.bl;

import javafx.util.Pair;
import layer.pl.PLException;
import layer.pl.PLInterface;
import layer.pl.UserDataBean;

import java.util.*;

import static java.lang.Math.min;

public class BLImpl implements BLInterface {
	PLInterface pl;
	public BLImpl(PLInterface pl) {
		this.pl = pl;
	}

	@Override
	public void register(String username, String password)
			throws DuplicateUserNameException, SimplePasswordException, PLException {
		UserDataBean user = pl.getData(username);
		if (user!=null) {
			throw new DuplicateUserNameException();
		}
		if (password==null || password.length()<8) {
			throw new SimplePasswordException();
		}
		pl.add(username, password);
	}

	@Override
	public UserDataBean login(String username, String password)throws PLException{
		UserDataBean user = pl.getData(username);
		if (user!=null && user.checkPassword(password)) {
			return user;
		} else {
			return null;
		}
	}

	@Override
	public UserDataBean edit(long loginId, String username, String newName,
			String newFamily)throws PLException{
		pl.edit(username, newName, newFamily);
		return pl.getData(username);
	}

	@Override
	public void logout(long loginId)throws PLException{
	}

	@Override
	public void unregister(long loginId, String username)throws PLException{
		pl.remove(username);
	}

	@Override
	public Map<String, Integer[][]> getLevelData(int level) throws PLException {
		String[] maps = pl.getLevelMaps(level);
		Integer[][] start = new Integer[5][5];
		Integer[][] end = new Integer[5][5];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				start[i][j] = maps[0].charAt(i*5 + j)-'0';
				end[i][j] = maps[1].charAt(i*5 + j)-'0';
			}
		}
		Map<String, Integer[][]> result = new HashMap<>();
		result.put("start", start);
		result.put("end", end);
		return result;
	}

	@Override
	public UserDataBean updateLevel(String username, int level, int moves, int time) throws PLException {
		UserDataBean userData = getUserDate(username);
		if(userData.getGameData(level)!=null) {
			time = min(userData.getGameData(level).getTimeRecord(), time);
			moves = min(userData.getGameData(level).getMovesRecord(), moves);
		}
		pl.updateGame(username,level,moves,time);
		return pl.getData(username);
	}

	@Override
	public Set<RecordBean> topTimeRecords(int untilLevel) throws PLException {
		Map<String,Integer> records = new HashMap<>();
		for (int i = untilLevel; i > 0; i--) {
			Map<String, Pair<Integer,Integer> > tmp = pl.getLevelPlays(i);
			for(String username: tmp.keySet()) {
				if(records.get(username)!=null)
					records.put(username,records.get(username)+tmp.get(username).getKey());
				else if(i==untilLevel)
					records.put(username, new Integer(tmp.get(username).getKey()));
			}
		}
		Set<RecordBean> sortedRecords = new HashSet<>();
		for(String username: records.keySet()) {
			sortedRecords.add(new RecordBean(username,records.get(username)));
		}
		return sortedRecords;
	}

	@Override
	public Set<RecordBean> topMovesRecord(int untilLevel) throws PLException {
		Map<String,Integer> records = new HashMap<>();
		for (int i = untilLevel; i > 0; i--) {
			Map<String, Pair<Integer,Integer> > tmp = pl.getLevelPlays(i);
			for(String username: tmp.keySet()) {
				if(records.get(username)!=null)
					records.put(username,records.get(username)+tmp.get(username).getValue());
				else if(i==untilLevel)
					records.put(username, new Integer(tmp.get(username).getValue()));
			}
		}
		Set<RecordBean> sortedRecords = new HashSet<>();
		for(String username: records.keySet()) {
			sortedRecords.add(new RecordBean(username,records.get(username)));
		}
		return sortedRecords;
	}

	private UserDataBean getUserDate(String username) throws PLException {
		return pl.getData(username);
	}

}
