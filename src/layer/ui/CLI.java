package layer.ui;

import java.util.Scanner;

import layer.bl.BLInterface;
import layer.bl.DuplicateUserNameException;
import layer.bl.SimplePasswordException;
import layer.pl.PLException;
import layer.pl.UserDataBean;

public class CLI {
	BLInterface bl;
	public CLI(BLInterface bl) {
		this.bl = bl;
	}
	public void start() {
		Scanner s = new Scanner(System.in);
		wloop: while(true) {
			System.out.println("Enter Command:");
			System.out.println("1: Login");
			System.out.println("2: Register");
			System.out.println("3: Quit");
			int c = Integer.parseInt(s.nextLine().trim());
			switch(c) {
			case 1:
				System.out.print("Enter name:");
				String uname = s.nextLine().trim();
				System.out.print("Enter password:");
				String upassword = s.nextLine().trim();
				try {
					UserDataBean logindata = bl.login(uname, upassword);
					if (logindata!=null) {
						innerloop:while(true) {
							System.out.println("Enter Command(" + logindata.getName() + "," + logindata.getFamily() + "):");
							System.out.println("1: Edit");
							System.out.println("2: Logout");
							System.out.println("3: Unsubscribe");
							int cc = Integer.parseInt(s.nextLine().trim());
							switch(cc) {
							case 1:
								System.out.print("Enter new name:");
								String newName = s.nextLine().trim();
								System.out.print("Enter new family:");
								String newFamily = s.nextLine().trim();
								logindata = bl.edit(logindata.getLoginId(), uname, newName, newFamily);
								System.out.println("user data updated successfully!");
								break;
							case 2:
								bl.logout(logindata.getLoginId());
								break innerloop;
							case 3: 
								bl.unregister(logindata.getLoginId(), uname);
								System.out.println("You have unsubscribed successfully!");
								break innerloop;
							}
						}
					} else {
						System.out.println("Invalid username or password");
					}
				} catch (PLException e) {
					System.out.println("Login failed: unknown persistency error!");
				}
				break;
			case 2:
				System.out.print("Enter name:");
				String name = s.nextLine().trim();
				System.out.print("Enter password:");
				String password = s.nextLine().trim();
				try {
					bl.register(name, password);
					System.out.println("you have registered successfully");
				} catch (DuplicateUserNameException e) {
					System.out.println("Register failed: user name is duplicate");
				} catch (SimplePasswordException e) {
					System.out.println("Register failed: password is simple");
				} catch (PLException e) {
					System.out.println("Register failed: unknown persistency error!");
				}
				break;
			case 3: break wloop;
			}
		}
	}

}
