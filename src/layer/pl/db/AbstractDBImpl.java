package layer.pl.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javafx.util.Pair;
import layer.pl.GameDataBean;
import layer.pl.PLException;
import layer.pl.PLInterface;
import layer.pl.UserDataBean;

public abstract class AbstractDBImpl implements PLInterface {

	@Override
	public void add(String username, String password) throws PLException {
		String query = "Insert into users(username,password,type)values('" +
				username + "', '" + password + "','player')";
		executeUpdate(query);
	}

	protected abstract Connection getConnection();

	@Override
	public void edit(String username, String name, String family)
			throws PLException {
		String query = "update users set name='"+name + "', family='"+family+
				"' where username='" +username + "'";
		executeUpdate(query);
	}

	private void executeUpdate(String query) throws PLException {
		Connection connection = null;
		try {
			connection = getConnection();
			Statement st = connection.createStatement();
			st.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PLException();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void remove(String username) throws PLException {
		String query = "delete from users where username='" +username + "'";
		executeUpdate(query);
		query = "delete from plays where username='" + username + "'";
		executeUpdate(query);
	}

	@Override
	public void updateGame(String username, int level, int moves, double time) throws PLException {
		String query = "delete from plays where (username='" + username + "' AND level='" + level +"')";
		executeUpdate(query);
		query = "insert into plays (username,level,moves,time)values('" + username + "', "
				+ level + ", " + moves + ", " + time + ")";
		executeUpdate(query);
	}

	@Override
	public UserDataBean getData(String username) throws PLException {
		String query = "select password from users where username='" +username + "'";
		
		Connection connection = null;
		try {
			connection = getConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(!rs.next())
				return  null;

			String password = rs.getString("password");
			query = "select level,moves,time from plays where username = '" + username + "'";
			rs = st.executeQuery(query);
			Map<Integer,GameDataBean> plays = new HashMap<>();
			while(rs.next()) {
				int level = rs.getInt("level");
				int moves = rs.getInt("moves");
				int time = rs.getInt("time");
				plays.put(level, new GameDataBean(moves,time));
			}
			return new UserDataBean(password,plays);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PLException();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public String[] getLevelMaps(int level) throws PLException{
		String query = "select * from levels where id='" + level + "'";
		String[] result = new String[2];
		Connection connection = null;
		try {
			connection = getConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(!rs.next())
				return  null;
			result[0] = rs.getString("start");
			result[1] = rs.getString("end");

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PLException();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return result;
		}
	}

	@Override
	public Map<String, Pair<Integer,Integer> > getLevelPlays(int level) throws PLException {
		String query = "select * from plays where level='" + level + "'";
		Map<String, Pair<Integer, Integer> > result = new HashMap<>();
		Connection connection = null;
		try {
			connection = getConnection();
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			while(rs.next()) {
				String username = rs.getString("username");
				int time = rs.getInt("time");
				int moves = rs.getInt("moves");
				result.put(username,new Pair<>(time,moves));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new PLException();
		} finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return result;
		}
	}

}
