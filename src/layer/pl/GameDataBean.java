package layer.pl;

public class GameDataBean {
    int movesRecord;
    int timeRecord;

    public GameDataBean(int movesRecord, int timeRecord) {
        this.movesRecord = movesRecord;
        this.timeRecord = timeRecord;
    }

    public int getMovesRecord() {
        return movesRecord;
    }

    public int getTimeRecord() {
        return timeRecord;
    }
}
