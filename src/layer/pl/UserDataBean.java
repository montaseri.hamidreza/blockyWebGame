package layer.pl;

import java.util.Map;

public class UserDataBean {
	String name, family,password;
	Map<Integer, GameDataBean> gamesData;
	public UserDataBean(String password, Map<Integer, GameDataBean> gamesData) {
		super();
		this.password = password;
		this.gamesData = gamesData;
	}

	public String getName() {
		return name;
	}

	public String getFamily() {
		return family;
	}

	public boolean checkPassword(String pass) {
		return password.equals(pass);
	}

	public GameDataBean getGameData(int level) {
		return gamesData.get(level);
	}

	public long getLoginId() {
		// TODO Auto-generated method stub
		return 0;
	}

}
