package layer.pl;


import javafx.util.Pair;

import java.util.Map;

public interface PLInterface {
	void add(String username, String password) throws PLException;
	void edit(String username, String name, String family) throws PLException;
	void remove(String username) throws PLException;
	void updateGame(String username, int level, int moves, double time) throws PLException;
	UserDataBean getData(String username) throws PLException;
	String[] getLevelMaps(int level) throws PLException;
    Map<String,Pair<Integer, Integer> > getLevelPlays(int level) throws PLException;
//	List<UserData> getAllUsers();
}
