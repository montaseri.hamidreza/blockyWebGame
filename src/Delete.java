import layer.bl.BLImpl;
import layer.bl.BLInterface;
import layer.pl.PLException;
import layer.pl.UserDataBean;
import layer.pl.db.SqliteDBImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "*.delete")
public class Delete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        BLInterface business = new BLImpl(new SqliteDBImpl());
        UserDataBean userData = null;
        try {
            userData = business.login(username,password);
            if(userData!=null) {
                request.getSession().invalidate();
                business.unregister(userData.getLoginId(),username);
                request.setAttribute("error","Your account is deleted successfully.");
                request.getRequestDispatcher("index.jsp").forward(request,response);
            }
        } catch (PLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
