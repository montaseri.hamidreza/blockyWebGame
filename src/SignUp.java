import layer.bl.BLImpl;
import layer.bl.BLInterface;
import layer.bl.DuplicateUserNameException;
import layer.bl.SimplePasswordException;
import layer.pl.PLException;
import layer.pl.UserDataBean;
import layer.pl.db.SqliteDBImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet(value = "*.signup")
public class SignUp extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        BLInterface business = new BLImpl(new SqliteDBImpl());
        try {
            business.register(username,password);
            request.setAttribute("error","Signed up successfully! You can sign in now.");
        } catch (PLException e) {
            e.printStackTrace();
            request.setAttribute("error","Sign up failed: Unknown error");
        } catch (SimplePasswordException e) {
            e.printStackTrace();
            request.setAttribute("error", "Sign up failed: Password is too simple");
        } catch (DuplicateUserNameException e) {
            e.printStackTrace();
            request.setAttribute("error", "Sign up failed: This username is taken");
        }
        request.getRequestDispatcher("index.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
