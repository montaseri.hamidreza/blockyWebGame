import layer.bl.BLImpl;
import layer.bl.BLInterface;
import layer.pl.PLException;
import layer.pl.UserDataBean;
import layer.pl.db.SqliteDBImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "*.finished")
public class Finished extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int level = Integer.parseInt(request.getParameter("level"));
        int time = Integer.parseInt(request.getParameter("time"));
        int moves = Integer.parseInt(request.getParameter("moves"));
        System.out.println(level+" "+time+" "+moves);
        BLInterface business = new BLImpl(new SqliteDBImpl());
        try {
            UserDataBean newUserData = business.updateLevel((String)request.getSession().getAttribute("username"),level,moves, time);
            request.getSession().setAttribute("userData",newUserData);
        } catch (PLException e) {
            e.printStackTrace();
        }
        String text = "done";
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(text);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
