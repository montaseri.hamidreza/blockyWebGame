import layer.bl.BLImpl;
import layer.bl.BLInterface;
import layer.pl.PLException;
import layer.pl.db.SqliteDBImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "*.play")
public class Play extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int level = Integer.parseInt(request.getParameter("level"));
        System.out.println("play!");
        BLInterface business = new BLImpl(new SqliteDBImpl());
        request.setAttribute("level",level);
        try {
            request.setAttribute("levelData",business.getLevelData(level));
        } catch (PLException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("game.jsp").forward(request,response);
    }
}
