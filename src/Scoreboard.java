import layer.bl.BLImpl;
import layer.bl.BLInterface;
import layer.pl.PLException;
import layer.pl.db.SqliteDBImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "*.scoreboard")
public class Scoreboard extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int untilLevel = Integer.parseInt(request.getParameter("until"));
        BLInterface business = new BLImpl(new SqliteDBImpl());
        try {
            request.setAttribute("topTimeRecords",business.topTimeRecords(untilLevel));
            request.setAttribute("topMovesRecords",business.topMovesRecord(untilLevel));
            request.setAttribute("untilLevel", untilLevel);
            request.getRequestDispatcher("scoreboard.jsp").forward(request,response);
        } catch (PLException e) {
            e.printStackTrace();
        }
    }
}
