import layer.bl.BLImpl;
import layer.bl.BLInterface;
import layer.pl.PLException;
import layer.pl.UserDataBean;
import layer.pl.db.SqliteDBImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "*.signin")
public class SignIn extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        BLInterface business = new BLImpl(new SqliteDBImpl());
        UserDataBean userData = null;
        try {
            userData = business.login(username,password);
        } catch (PLException e) {
            e.printStackTrace();
        }
        if(userData == null) {
            request.setAttribute("error", "Sign in failed! :( Try again.");
            request.getRequestDispatcher("index.jsp").forward(request,response);
        } else {
            request.getSession().setAttribute("levels", 15);//TODO: Get from database
            request.getSession().setAttribute("userData", userData);
            request.getSession().setAttribute("username", username);
            request.getRequestDispatcher("user.jsp").forward(request,response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
